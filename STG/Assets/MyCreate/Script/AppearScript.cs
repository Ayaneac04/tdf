﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppearScript : MonoBehaviour
{

    [SerializeField] private VRMSetting vrmset;
    //　出現させる敵を入れておく
    [SerializeField] GameObject[] enemys;
    //　次に敵が出現するまでの時間
    [SerializeField] float appearNextTime;
    //　この場所から出現する敵の数
    [SerializeField] int maxNumOfEnemys;
    //　今何人の敵を出現させたか
    private int numberOfEnemys;
    //　待ち時間計測フィールド
    private float elapsedTime;

    public VRMSetting loadPlayer;

    // Use this for initialization
    private void Start()
    {
        numberOfEnemys = 0;
        elapsedTime = 0f;
        vrmset = GameObject.Find("VRMScript").GetComponent<VRMSetting>();
    }

    // Update is called once per frame
    private void Update () {
        //　この場所から出現する最大数を超えてたら何もしない
        if (numberOfEnemys >= maxNumOfEnemys)
        {
            return;
        }
        //　経過時間を足す
        elapsedTime += Time.deltaTime;

        if (!loadPlayer.GetComponent<VRMSetting>().PlayerF) {
            //　経過時間が経ったら
            if (elapsedTime > appearNextTime)
            {
                elapsedTime = 0f;

                AppearEnemy();
            }
        }
    }

    //　敵出現メソッド
    private void AppearEnemy()
    {
        //　出現させる敵をランダムに選ぶ
        var randomValue = Random.Range(0, enemys.Length);
        //　敵の向きをランダムに決定
        var randomRotationY = 0;

        GameObject.Instantiate(enemys[randomValue], transform.position, Quaternion.Euler(0f, randomRotationY, 0f));

        numberOfEnemys++;
        elapsedTime = 0f;
    }
}
