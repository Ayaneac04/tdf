﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CreatePlayer : MonoBehaviour {

    [SerializeField] private VRMSetting vrmset;

    public GameObject VRM_Player;

    public GameObject LoadPoint;

    Animator vrm_anim;

    public GameObject GunObj;

    public GameObject BulletObj;

    public LoadPointPosition pos;

    public bool PlayerF = true;

    // Use this for initialization
    void Start () {
        vrmset = GameObject.Find("VRMScript").GetComponent<VRMSetting>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void CratePlayer()
    {

        VRM_Player = vrmset.VRM_Resources;

        //プレイヤーロードのフラグ
        PlayerF = true;

        vrm_anim = VRM_Player.GetComponent<Animator>();

        VRM_Player.AddComponent<ShootingAnimation>();

        VRM_Player.AddComponent<NavMeshAgent>();

        //右手のボーン座標を取得
        Transform rhand = vrm_anim.GetBoneTransform(HumanBodyBones.RightHand);

        //銃のオブジェクトを取得したボーン座標に設置
        GunObj = Instantiate(GunObj, rhand.transform.position, rhand.transform.rotation);
        VRM_Player.GetComponent<ShootingAnimation>().gun = GunObj;
        VRM_Player.GetComponent<ShootingAnimation>().m_live = PlayerF;
        VRM_Player.GetComponent<ShootingAnimation>().m_Bullet = BulletObj;
        GunObj.transform.parent = vrm_anim.GetBoneTransform(HumanBodyBones.RightHand);

        //プレイヤーの位置に生成
        VRM_Player.transform.position = LoadPoint.transform.position;
        VRM_Player.transform.eulerAngles = LoadPoint.transform.eulerAngles;

        Instantiate(VRM_Player, VRM_Player.transform.position, Quaternion.Euler(VRM_Player.transform.eulerAngles));

    }
}
