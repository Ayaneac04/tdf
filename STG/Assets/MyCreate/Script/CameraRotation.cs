﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotation : MonoBehaviour {

    public Transform verRot;
    public Transform horRot;

    public MovementController MCon;

    // Use this for initialization
    void Start () {
        verRot = transform.parent;
        horRot = GetComponent<Transform>();
    }
	
	// Update is called once per frame
	void Update () {

        if (MCon.CanMove)
        {
            // カメラの回転(マウス移動)
            float X_Rotation = Input.GetAxis("Mouse X");
            float Y_Rotation = Input.GetAxis("Mouse Y");
            verRot.transform.Rotate(0, X_Rotation, 0);
            horRot.transform.Rotate(-Y_Rotation, 0, 0);
        }
    }
}
