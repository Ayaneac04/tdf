﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public enum EnemyState
{
    Dead,
    Walk,
    Wait,
}

public class EnemyControl : MonoBehaviour
{
    //　エネミーの移動
    private CharacterController enemyController;
    //  色
    public Material e_material;
    //　発射口
    public Transform m_Muzzle;                               
    //　弾速
    public float m_Speed = 500;
    //　弾
    public GameObject e_Bullet;   
    //　リスポン位置
    private SetPosition e_setPosition;
    //　アニメーション
    private Animator e_animator;
    //　目的地
    private Vector3 e_destination;
    //　速度
    private Vector3 e_velocity;
    //　移動方向
    private Vector3 e_direction;
    //　エージェント
    [SerializeField]
    private NavMeshAgent e_agent;
    //　経過時間
    private float e_elapsedTime;
    //　体力
    private int e_Hp;
    //　到着フラグ
    private bool e_arrived;
    //　敵の状態
    [SerializeField]
    private EnemyState e_state;
    //　ターゲットキャラクター
    [SerializeField]
    private Transform e_playerTransform;
    //　発射間隔
    private float e_FiringTime = 0f;
    //　歩くスピード
    [SerializeField]
    private float e_walkSpeed = 1.0f;
    //　回転スピード
    [SerializeField]
    private float e_rotateSpeed = 45f;
    //　待ち時間
    [SerializeField]
    private float e_waitTime = 5f;
    //　弾を飛ばす間隔時間
    [SerializeField]
    private float m_FiringInterval = 0.3f;   
  

    // Use this for initialization
    void Start()
    {
        enemyController = GetComponent<CharacterController>();
        e_animator = GetComponent<Animator>();
        e_setPosition = GetComponent<SetPosition>();
        e_setPosition.CreateRandomPosition();
        e_velocity = Vector3.zero;
        e_playerTransform = GameObject.FindGameObjectWithTag("pChar").transform;
        e_arrived = false;
        e_elapsedTime = 0f;
        SetState("wait");
        e_agent = GetComponent<NavMeshAgent>();

        e_walkSpeed = 1.0f;
        e_state = EnemyState.Wait;
        e_Hp = 2;
    }

    // Update is called once per frame
    void Update()
    {
        if (e_agent.pathStatus != NavMeshPathStatus.PathInvalid)
        {
            //　navMeshAgentの操作
            //　的に向かって動く（ナビメッシュ使用）
            if (e_state == EnemyState.Walk)
            {
                //　目的地に到着したかどうかの判定
                if (e_agent.remainingDistance < 0.7f)
                {
                    SetState("wait");
                }
                else if(e_agent.remainingDistance < 10.0f && e_agent.remainingDistance > 1.0f)
                {
                    //　プレイヤーの位置をセット
                    e_setPosition.SetDestination(e_playerTransform.position);
                    //　プレイヤーに向けて移動
                    e_agent.SetDestination(e_setPosition.GetDestination());
                    //　プレイヤーの方向を取得
                    var playerDirection = new Vector3(e_playerTransform.position.x, transform.position.y, e_playerTransform.position.z) - transform.position;
                    //　敵の向きをプレイヤーの方向に少しづつ変える
                    var dir = Vector3.RotateTowards(transform.forward, playerDirection, e_rotateSpeed * Time.deltaTime, 0f);
                    //　算出した方向の角度を敵の角度に設定
                    transform.rotation = Quaternion.LookRotation(dir);
                    e_FiringTime += Time.deltaTime;
                    if (e_FiringTime < m_FiringInterval)
                    {
                        return;
                    }
                    //  射撃
                    Shotting();
                }
            }
            //　到着していたら一定時間待つ
            else if (e_state == EnemyState.Wait)
            {
                e_elapsedTime += Time.deltaTime;

                //　待ち時間を越えたら次の目的地を設定
                if (e_elapsedTime > e_waitTime)
                {
                    e_playerTransform = GameObject.FindGameObjectWithTag("pChar").transform;
                    SetState("walk");
                    e_elapsedTime = 0f;
                }
            }
            e_velocity.y += Physics.gravity.y * Time.deltaTime;
            enemyController.Move(e_velocity * e_walkSpeed * Time.deltaTime);
        }
    }

    /// <summary>
    /// 敵キャラクターの状態変更メソッド
    /// </summary>
    public void SetState(string mode, Transform obj = null)
    {
        //　死んでたら状態変更しない
        if (e_state == EnemyState.Dead)
        {
            return;
        }

        if (mode == "walk")
        {
            e_arrived = false;
            e_elapsedTime = 0f;
            e_state = EnemyState.Walk;
            e_setPosition.SetDestination(e_playerTransform.position);
            e_animator.SetBool("e_run", true);
            e_agent.SetDestination(e_setPosition.GetDestination());
            e_agent.isStopped = false;
        }
        else if (mode == "wait")
        {
            e_elapsedTime = 0f;
            e_state = EnemyState.Wait;
            e_arrived = true;
            e_animator.SetBool("e_run", false);
            e_agent.isStopped = true;
        }
        else if (mode == "dead")
        {
            e_state = EnemyState.Dead;
            e_agent.isStopped = true;
            Destroy(this.gameObject);
        }
    }


    /// <summary>
    /// 射撃
    /// </summary>
    void Shotting()
    {

        // 弾丸の複製
        GameObject bullets = Instantiate(e_Bullet) as GameObject;

        Vector3 force;

        force = gameObject.transform.forward * m_Speed;

        // Rigidbodyに力を加えて発射
        bullets.GetComponent<Rigidbody>().AddForce(force);

        // 弾丸の位置を調整
        bullets.transform.position = m_Muzzle.position;

        e_FiringTime = 0f;

    }

    void OnCollisionEnter(Collision collision)
    {
        //　衝突判定
        //　相手のタグが弾ならば、ダメージを受ける
        if (collision.gameObject.tag == "Bullet")
        {
            
           e_Hp--;
           if (e_Hp <= 0)
           {
                SetState("dead");
           }
            
        }
        //　衝突判定
        if (collision.gameObject.tag == "Player")
        {
            Destroy(this.gameObject);
        }
    }
}