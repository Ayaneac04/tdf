﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class addscene : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private bool isLoaded = false;

    public void OnClick()
    {
        isLoaded = !isLoaded;
        if (isLoaded)
        {
            SceneManager.LoadSceneAsync("01", LoadSceneMode.Additive);
        }
        else
        {
            SceneManager.LoadSceneAsync("01");
            Resources.UnloadUnusedAssets();
        }
    }
}
