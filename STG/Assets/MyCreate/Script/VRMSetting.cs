﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;
using UnityEngine.AI;
using System.Windows.Forms; //OpenFileDialog用に使う

public class VRMSetting : MonoBehaviour
{
    public GameObject VRM_Player;
    GameObject VRM_Root;

    public GameObject VRM_Resources;
    Animator vrm_anim;
    public RuntimeAnimatorController Set_anim;

    //public GameObject PlayerLoadPoint;
    public GameObject LoadPoint;

    public GameObject GunObj;

    public GameObject BulletObj;

    public LoadPointPosition pos;

    public bool PlayerF = true;

    /// <summary>
    /// 自機生成
    /// </summary>
    public void PlayerVRM()
    {
        //VRM_PlayerオブジェクトをVRMの親にする
        VRM_Root = new GameObject();
        //プレイヤーロードのフラグ
        PlayerF = true;
        //任意のVRMファイルを選択
        string path = EditorUtility.OpenFilePanel("Overwrite with vrm", "", "vrm");
        //VRM読み込み
        StartCoroutine(LoadVrmCoroutine(path));
        //VRMを生成する場所の取得
        LoadPoint.transform.position = pos.transform.position;
        //プレイヤーの位置に生成
        VRM_Root.transform.position = LoadPoint.transform.position;
        
    }

    /// <summary>
    ///  自機以外のVRMを生成
    /// </summary>
    public void CreateVRM()
    {
        //新規オブジェクトを生成してVRMの親にする
        VRM_Root = new GameObject();

        //任意のVRMファイルを選択
        OpenFileDialog open_file_dialog = new OpenFileDialog();

        open_file_dialog.Filter = "vrmファイル|*.vrm";

        //ダイアログを開く
        open_file_dialog.ShowDialog();

        //プレイヤーロードのフラグ
        PlayerF = true;

        //取得したファイル名をstringに代入する
        string path = open_file_dialog.FileName;
        //VRM読み込み
        StartCoroutine(LoadVrmCoroutine(path));
        //プレイヤーの位置に生成
        VRM_Root.transform.position = LoadPoint.transform.position;
        VRM_Root.transform.eulerAngles = LoadPoint.transform.eulerAngles;

    }


    IEnumerator LoadVrmCoroutine(string path)
    {     
        var www = new WWW("file://" + path);
        yield return www;
        VRM.VRMImporter.LoadVrmAsync(www.bytes, OnLoaded);
    }

    /// <summary>
    /// VRMのロード
    /// </summary>
    /// <param name="vrm"></param>
    void OnLoaded(GameObject vrm)
    {
        VRM_Resources = vrm;
        if (VRM_Root != null)
        {
            vrm.transform.SetParent(VRM_Root.transform, false);  
        }

        //生成したVRMからAnimatorを取得
        vrm_anim = VRM_Resources.GetComponent<Animator>();
        //AnimatorControllerに専用のコントローラーをセット
        vrm_anim.runtimeAnimatorController = Set_anim;

        VRM_Resources.tag = "pChar";


        //読み込みが終わったらスクリプト停止機能を解除する
        if (PlayerF)
        {
            PlayerF = false;
        }
    }
}