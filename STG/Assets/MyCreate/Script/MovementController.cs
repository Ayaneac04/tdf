﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour {

    public bool CanMove = true;                    //カメラ移動フラグ
    public bool CanMoveForward = true;             //カメラ移動前
    public bool CanMoveBack = true;                //カメラ移動後ろ
    public bool CanMoveLeft = true;                //カメラ移動左
    public bool CanMoveRight = true;               //カメラ移動右
    public bool CanMoveUp = true;                  //カメラ移動上
    public bool CanMoveDown = true;                //カメラ移動下

    public float MovementSpeed = 10f;
    public float RotationSpeed = 10f;

    public bool CamMoveFlag;

    public LoadPointPosition pos;

    void Start()
    {
        Application.targetFrameRate = 60;
        CamMoveFlag = true;
    }

    void Update()
    {

    }

    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.Z))
        {
            CanMove = false;
        }
        else if (Input.GetKey(KeyCode.X))
        {
            CanMove = true;
        }


        if (CanMove)
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Confined;
            UpdatePosition();
        }
        else
        {
            Cursor.visible = true;
            SetVMR();
        }
    }

    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    //                          カメラ
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    void UpdatePosition()
    {

        // 移動キーのチェック
        int[] input = new int[6]; // Forward, Back, Left, Right, Up, Down
        if (CanMoveForward && Input.GetKey(KeyCode.W))
        {
            input[0] = 1;
        }
        else if (CanMoveBack && Input.GetKey(KeyCode.S))
        {
            input[1] = 1;
        }
        if (CanMoveLeft && Input.GetKey(KeyCode.A))
        {
            input[2] = 1;
        }
        else if (CanMoveRight && Input.GetKey(KeyCode.D))
        {
            input[3] = 1;
        }
        if (CanMoveUp && Input.GetKey(KeyCode.Q))
        {
            input[4] = 1;
        }
        else if (CanMoveDown && Input.GetKey(KeyCode.E))
        {
            input[5] = 1;
        }
        //カメラ回転リセット
        if (Input.GetKey(KeyCode.C))
        {
            transform.localRotation = Quaternion.identity;
        }
        int numInput = 0;
        for (int i = 0; i < 6; i++)
        {
            numInput += input[i];
        }

        // Add velocity to the gameobject
        float curSpeed = numInput > 0 ? MovementSpeed : 0;
        Vector3 AddPos = input[0] * Vector3.forward + input[2] * Vector3.left + input[4] * Vector3.up
            + input[1] * Vector3.back + input[3] * Vector3.right + input[5] * Vector3.down;
        AddPos = GetComponent<Rigidbody>().rotation * AddPos;
        GetComponent<Rigidbody>().velocity = AddPos * (Time.fixedDeltaTime * curSpeed);

    }


    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    //                      オブジェクト生成
    //＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    void SetVMR()
    {
        if (Input.GetMouseButtonDown(0))
        {
            
        }

    }
}
