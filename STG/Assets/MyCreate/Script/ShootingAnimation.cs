﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ShootingAnimation : MonoBehaviour {

    [SerializeField] private float m_FiringInterval = 0.3f;     //　弾を飛ばす間隔時間
    [SerializeField]private NavMeshAgent p_agent;
    [SerializeField]private float rotateSpeed = 45f;            //　回転スピード
    [SerializeField]private Transform enemyTransform;        //　ターゲットキャラクター
    private float p_FiringTime = 0f;

    private bool m_Aim;
    private float m_YRotation;
    private Vector3 m_MoveDir = Vector3.zero;
    private CollisionFlags m_CollisionFlags;
    private bool m_PreviouslyGrounded;
    private bool m_reload;
    public Animator anim;                                    //プレイキャラのアニメーション
    private Animator m_animator;                             //プレイキャラのアニメーション
    public GameObject m_Bullet;                              //弾
    public GameObject gun;                                   //銃
    public Transform m_Muzzle;                               //発射口
    public float m_Speed = 500;                              //弾速
    private float m_ElapsedTime = 0f;                        //射撃間隔の時間
    private int m_AmmoCount = 30;                            //リロード時間
    private int m_Hp;                                        //体力
    public bool m_live;                                   　 //生存
    private ParticleSystem m_muzzlef;                        //マズルフラッシュ
    private GameObject Target;
    private int EffectTime_On = 2;
    private int EffectTime_Off = 0;

    // Use this for initialization
    void Start () {
     
        m_Aim = false;
        m_reload = false;
        m_Hp = 3;
        anim = GetComponent<Animator>();
        m_Muzzle = gun.FindDeep("Muzzle").transform;
        enemyTransform = GameObject.FindGameObjectWithTag("Enemy").transform;
        p_agent = GetComponent<NavMeshAgent>();
    }
	
	// Update is called once per frame
    /// <summary>
    /// 射撃職の一連動作管理
    /// </summary>
	void Update () {
        if (m_live)
        {
            if(p_agent.remainingDistance < 50.0f && p_agent.remainingDistance > 1.0f)
            {
                m_Aim = true; 
            }
            else
            {
                m_Aim = false;
            }

            if (m_Aim)
            {
                anim.SetBool("is_aim", true);
            }
            else
            {
                anim.SetBool("is_aim", false);
            }

            if (m_Aim && !m_reload)
            {
                //　プレイヤーの方向を取得
                var playerDirection = new Vector3(enemyTransform.position.x, transform.position.y, enemyTransform.position.z) - transform.position;
                //　敵の向きをプレイヤーの方向に少しづつ変える
                var dir = Vector3.RotateTowards(transform.forward, playerDirection, rotateSpeed * Time.deltaTime, 0f);
                //　算出した方向の角度を敵の角度に設定
                transform.rotation = Quaternion.LookRotation(dir);
                p_FiringTime += Time.deltaTime;
                if (p_FiringTime < m_FiringInterval)
                {
                    return;
                }
                Shotting();
            }
            else
            {
               // var Emission = m_muzzlef.emission;

              //  Emission.rateOverTime = EffectTime_Off;
                anim.SetBool("is_shotting", false);
            }
            if (m_reload)
            {
                m_AmmoCount = 30;
                m_reload = false;
                m_Aim = true;
            }
        }

    }


    /// <summary>
    /// 銃の射撃プログラム
    /// </summary>
    private void Shotting()
    {

       // var Emission = m_muzzlef.emission;

        //Emission.rateOverTime = EffectTime_On;

        anim.SetBool("is_shotting", true);
        // 弾丸の複製
        GameObject bullets = Instantiate(m_Bullet) as GameObject;

        Vector3 force;

        force = gameObject.transform.forward * m_Speed;
        Rigidbody rb = bullets.GetComponent<Rigidbody>();
        // Rigidbodyに力を加えて発射
        rb.AddForce(force);

        // 弾丸の位置を調整
        bullets.transform.position = m_Muzzle.position;

        m_ElapsedTime = 0f;
        m_AmmoCount--;
        if (m_AmmoCount <= 0)
        {
            m_reload = true;
            m_Aim = false;
        }
    }
}
